# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

- 第1步：创建一个新的用户，并给予该用户可以访问数据库和创建视图的权限，同时控制该用户使用的存储空间。该用户的名字是 sale，密码是 123，默认表空间为 users，并且有一个额外的 con_res_role 角色，该角色有特定的权限。

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE con_res_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
--收回角色
REVOKE con_res_role FROM sale;
```
![](1.png)
![](2.png)
![](3.png)
![](4.png)
- 第2步：在 sale 用户下创建一个名为 customers 的表，并将其存储在 USERS 表空间中。然后，创建一个名为 customers_view 的视图，该视图查询 customers 表的 name 列。最后，授予 hr 用户对 customers_view 视图的 SELECT 权限，以允许该用户查询该视图的内容。

```sql
$ sqlplus sale/123@pdborcl
SQL> show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang
```
![](5.png)
- 第3步：使用 hr 用户身份查询 sale 用户所拥有的对象。但是由于 hr 用户没有对 sale 用户的 customers 表的访问权限，因此查询失败。但是 hr 用户具有对 customers_view 视图的 SELECT 权限，因此查询视图时没有出现错误并返回视图的内容。

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers;
select * from sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM sale.customers_view;
NAME
--------------------------------------------------
zhang
wang
```
![](6.png)
## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sql
$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

使用 sqlplus 工具连接到名为 pdborcl 的数据库，并使用 system 用户名和密码进行身份验证。
查询名为 USERS 的表空间中的数据文件的名称、大小、最大大小和是否可自动扩展等信息。
查询所有表空间的大小、剩余空间、使用空间和使用率。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```
![](7.png)
![](8.png)
![](8.png)
- 第一个查询用于查看名为 USERS 的表空间的数据文件的信息，包括文件名、大小、最大大小和是否自动扩展。该查询使用 dba_data_files 和 dba_free_space 视图。

- 第二个查询用于查看所有表空间的信息，包括表空间名称、总大小、剩余空间、使用空间和使用率。该查询使用 dba_free_space 和 dba_data_files 视图。

- 这些操作的目的是查询数据库的表空间和数据文件的信息，以便监控和管理数据库的存储使用情况。


## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;
```
![](10.png)
## 实验总结
> 通过本次实验，我掌握了Oracle数据库的用户管理和权限控制的基本原理和操作技能，对于今后在数据库开发和管理方面的工作有很大的帮助。
- 在本次实验中，首先创建了一个新的本地角色 con_res_role，该角色包含了 connect 和 resource 角色，同时也包含了 CREATE VIEW 权限。这样，任何拥有 con_res_role 的用户就同时拥有这三种权限。这个过程让我学会了如何创建角色以及如何为角色授权。

- 接下来，在创建角色之后，我创建了一个名为 sale 的新用户，为其分配了表空间并设置了限额为50M，然后将 con_res_role 角色授予给了该用户。这个过程让我学会了如何创建用户、如何分配表空间以及如何将角色授予给用户。

- 最后，在测试环节，我使用新用户 sale 连接数据库，创建了一个名为 customers 的表，插入了数据，创建了一个名为 customers_view 的视图，并成功查询了表和视图的数据。这个过程让我学会了如何在新用户下创建表和视图，并测试其能否正常使用。