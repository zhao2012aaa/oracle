﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

- - 

表及表空间设计方案：

```
-- 创建 DATA_TBS 表空间，用于存储数据
CREATE TABLESPACE DATA_TBS
DATAFILE 'data_tbs.dat' SIZE 100M
AUTOEXTEND ON NEXT 10M
MAXSIZE UNLIMITED;

-- 创建 INDEX_TBS 表空间，用于存储索引
CREATE TABLESPACE INDEX_TBS
DATAFILE 'index_tbs.dat' SIZE 100M
AUTOEXTEND ON NEXT 10M
MAXSIZE UNLIMITED;

-- 创建 CUSTOMERS 表，包括顾客 ID、姓名、电子邮件和电话号码
CREATE TABLE CUSTOMERS (
  CUSTOMER_ID NUMBER(10) PRIMARY KEY,
  NAME VARCHAR2(50) NOT NULL,
  EMAIL VARCHAR2(50),
  PHONE VARCHAR2(15)
)
TABLESPACE DATA_TBS;

-- 创建 PRODUCTS 表，包括产品 ID、名称、价格和库存数量
CREATE TABLE PRODUCTS (
  PRODUCT_ID NUMBER(10) PRIMARY KEY,
  NAME VARCHAR2(100) NOT NULL,
  PRICE NUMBER(10, 2) NOT NULL,
  STOCK NUMBER(10) NOT NULL
)
TABLESPACE DATA_TBS;

-- 创建 ORDERS 表，包括订单 ID、顾客 ID、订单日期
CREATE TABLE ORDERS (
  ORDER_ID NUMBER(10) PRIMARY KEY,
  CUSTOMER_ID NUMBER(10) NOT NULL,
  ORDER_DATE DATE NOT NULL,
  FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS(CUSTOMER_ID)
)
TABLESPACE DATA_TBS;

-- 创建 ORDER_ITEMS 表，包括订单项 ID、订单 ID、产品 ID、数量
CREATE TABLE ORDER_ITEMS (
  ORDER_ITEM_ID NUMBER(10) PRIMARY KEY,
  ORDER_ID NUMBER(10) NOT NULL,
  PRODUCT_ID NUMBER(10) NOT NULL,
  QUANTITY NUMBER(10) NOT NULL,
  FOREIGN KEY (ORDER_ID) REFERENCES ORDERS(ORDER_ID),
  FOREIGN KEY (PRODUCT_ID) REFERENCES PRODUCTS(PRODUCT_ID)
)
TABLESPACE DATA_TBS;
```

以上代码创建了两个表空间，一个用于存储数据，一个用于存储索引。然后创建了四张表，每张表都分配到数据表空间中。

插入数据，每张表三万条数据，四张表共12万条数据。

首先向PRODUCTS表插入虚拟数据，这些数据包括产品ID、产品名称、价格和数量。具体流程如下：

- 使用FOR循环语句，从1到30,000遍历每个整数i。
- 在每个迭代中，使用INSERT INTO语句将一条新记录插入PRODUCTS表中，其中包含productid（即i）、productname（格式为“Product i”）、price（在10到100之间随机生成）和quantity（在1到100之间随机生成）。还使用SELECT语句从dual表中选择一行，同时使用子查询检查是否已经存在具有相同productid的记录。如果存在，则不执行插入操作。
- 为了避免过大的事务大小，每100次迭代时进行一次COMMIT提交。

```
-- 插入虚拟数据到products表
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO products (productid, productname, price, quantity)
    SELECT i AS productid, 'Product ' || i AS productname, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price, ROUND(DBMS_RANDOM.VALUE(1, 100)) AS quantity
    FROM dual
    WHERE NOT EXISTS (SELECT 1 FROM products WHERE productid = i);
  
    -- Commit periodically to avoid excessive transaction size
    IF MOD(i, 100) = 0 THEN
      COMMIT;
    END IF;
  END LOOP;
  
  COMMIT;
END;
/
```



会向CUSTOMER表插入虚拟数据，具体流程如下：

- 使用FOR循环语句，从1到30,000遍历每个整数i。
- 在每个迭代中，使用INSERT INTO语句将一条新记录插入CUSTOMER表中，其中包含customerid（即i）、customername（格式为“Customer i”）和email（格式为“customeri@example.com”）。还使用SELECT语句从dual表中选择一行，同时使用子查询检查是否已经存在具有相同customerid的记录。如果存在，则不执行插入操作。
- 不需要在循环外显式提交事务，因为每次循环都是一个单独的事务

```
-- 插入虚拟数据到customers表
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO customer (customerid, customername, email)
    SELECT i AS customerid, 'Customer ' || i AS customername, 'customer' || i || '@example.com' AS email
    FROM dual
    WHERE NOT EXISTS (
      SELECT 1
      FROM customer
      WHERE customerid = i
    );
  END LOOP;
  COMMIT;
END;
/
```

!

向ORDERS表插入虚拟数据，具体流程如下：

- 使用FOR循环语句，从1到30,000遍历每个整数i。
- 在每个迭代中，使用INSERT INTO语句将一条新记录插入ORDERS表中，其中包含orderid（即i）、orderdate（在当前日期基础上随机减去1到365之间的天数）和customerid（在1到1000之间随机选择）。还使用SELECT语句从dual表中选择一行，同时使用子查询检查是否已经存在具有相同orderid的记录。如果存在，则不执行插入操作。
- 不需要在循环外显式提交事务，因为每次循环都是一个单独的事务

````
-- 插入虚拟数据到orders表
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO orders (orderid, orderdate, customerid)
    SELECT i AS orderid, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)) AS orderdate, ROUND(DBMS_RANDOM.VALUE(1, 1000)) AS customerid
    FROM dual
    WHERE NOT EXISTS (
      SELECT 1
      FROM orders
      WHERE orderid = i
    );
  END LOOP;
  COMMIT;
END;
/
````



向ORDERTETAILS表插入虚拟数据，具体流程如下：

- 使用FOR循环语句，从1到30,000遍历每个整数i。
- 在每个迭代中，使用MERGE INTO语句将一条新记录插入ORDERTETAILS表中。首先，使用SELECT语句从dual表中选择一行，并生成具有以下字段的数据：orderid（即i）、productid（在1到30,000之间随机选择）、quantity（在1到10之间随机选择）和price（在10到100之间随机生成）。然后，使用MERGE INTO语句将这条数据与ORDERTETAILS表进行合并。如果存在orderid和productid都匹配的记录，则不执行任何操作。否则，插入一个新记录，其中包含orderid、productid、quantity和price。
- 不需要在循环外显式提交事务，因为每次循环都是一个单独的事务。

```
-- 插入虚拟数据到ORDERTETAILS表
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO ORDERTETAILS od
    USING (
      SELECT i AS orderid, ROUND(DBMS_RANDOM.VALUE(1, 30000)) AS productid, ROUND(DBMS_RANDOM.VALUE(1, 10)) AS quantity, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price
      FROM dual
    ) data
    ON (od.orderid = data.orderid AND od.productid = data.productid)
    WHEN NOT MATCHED THEN
      INSERT (orderid, productid, quantity, price)
      VALUES (data.orderid, data.productid, data.quantity, data.price);
  END LOOP;
  COMMIT;
END;
/
```



权限及用户分配方案：

```
-- 创建 SALES_USER 用户，用于销售人员
CREATE USER SALES_USER IDENTIFIED BY sales_password
DEFAULT TABLESPACE DATA_TBS
TEMPORARY TABLESPACE TEMP;

-- 创建 MANAGER_USER 用户，用于管理人员
CREATE USER MANAGER_USER IDENTIFIED BY manager_password
DEFAULT TABLESPACE DATA_TBS
TEMPORARY TABLESPACE TEMP;

-- 给 SALES_USER 用户授予 CONNECT 和 RESOURCE 权限
GRANT CONNECT, RESOURCE TO SALES_USER;

-- 给 MANAGER_USER 用户授予 CONNECT、RESOURCE 和 DBA 权限
GRANT CONNECT, RESOURCE, DBA TO MANAGER_USER;
```

以上代码创建了两个用户，一个用于销售人员，一个用于管理人员。销售人员用户被授予 CONNECT 和 RESOURCE 权限，而管理人员用户被授予 CONNECT、RESOURCE 和 DBA 权限。

PL/SQL程序包设计：

```
-- 创建 SALES_PKG 程序包，包括 GET_TOTAL_SALES 和 UPDATE_STOCK 两个函数
CREATE OR REPLACE PACKAGE SALES_PKG IS
  FUNCTION GET_TOTAL_SALES(P_PRODUCT_ID NUMBER) RETURN NUMBER;
  PROCEDURE UPDATE_STOCK(P_PRODUCT_ID NUMBER, P_QUANTITY NUMBER);
END SALES_PKG;

-- 创建 SALES_PKG 程序包的实现
CREATE OR REPLACE PACKAGE BODY SALES_PKG IS
  FUNCTION GET_TOTAL_SALES(P_PRODUCT_ID NUMBER) RETURN NUMBER IS
    TOTAL_SALES NUMBER(10, 2);
  BEGIN
    SELECT SUM(OI.QUANTITY * P.PRICE)
    INTO TOTAL_SALES
    FROM ORDER_ITEMS OI, PRODUCTS P
    WHERE OI.PRODUCT_ID = P.PRODUCT_ID
    AND P.PRODUCT_ID = P_PRODUCT_ID;

    RETURN TOTAL_SALES;
  END GET_TOTAL_SALES;

  PROCEDURE UPDATE_STOCK(P_PRODUCT_ID NUMBER, P_QUANTITY NUMBER) IS
  BEGIN
    UPDATE PRODUCTS
    SET STOCK = STOCK - P_QUANTITY
    WHERE PRODUCT_ID = P_PRODUCT_ID;
  END UPDATE_STOCK;
END SALES_PKG;
```

以上代码创建了一个名为 SALES_PKG 的程序包，其中包含 GET_TOTAL_SALES 和 UPDATE_STOCK 两个函数。GET_TOTAL_SALES 函数用于计算某个产品的总销售额，而 UPDATE_STOCK 函数用于更新某个产品的库存数量。

数据库备份方案：

备份方案包括定期进行全备份、定期进行增量备份、定期备份控制文件和服务器参数文件，以及将备份文件存储在多个地点。

```
# 全备份
rman TARGET / NOCATALOG
RUN {
  BACKUP DATABASE PLUS ARCHIVELOG;
  BACKUP CURRENT CONTROLFILE;
  BACKUP SPFILE;
}

# 增量备份
rman TARGET / NOCATALOG
RUN {
  BACKUP INCREMENTAL LEVEL 1 DATABASE PLUS ARCHIVELOG;
}
```

以上是使用 RMAN 工具进行全备份和增量备份的示例命令。全备份包括备份整个数据库，同时备份控制文件和服务器参数文件。增量备份仅备份自上次备份以来更改的数据。为了防止硬件故障，备份文件应该存储在不同的物理设备上。

最终结果是一个基于Oracle数据库的商品销售系统的数据库设计方案，包括表及表空间设计方案、权限及用户分配方案、PL/SQL程序包设计和数据库备份方案。



**总结与心得**

本实验要求设计一套基于Oracle数据库的商品销售系统的数据库设计方案，包括表及表空间设计方案、权限及用户分配方案、存储过程和函数的设计以及数据库备份方案。在实验过程中，我学习了以下内容：

1. Oracle数据库的基本概念和架构，包括实例、表空间、表、索引等。
2. 如何使用SQL语言进行数据库设计，包括创建表、插入数据、查询数据、更新数据和删除数据等操作。
3. 如何使用PL/SQL语言进行存储过程和函数的设计，实现复杂的业务逻辑。
4. 如何进行Oracle数据库的备份和恢复。

通过本实验，我深入了解了Oracle数据库的使用和设计，掌握了数据库设计的基本方法和技巧，提高了自己的实际操作能力。同时，我也意识到了数据安全的重要性，必须合理设计数据库的权限和备份方案，以保证数据的安全性和完整性。

总的来说，本实验是一次非常有收获的实践，让我更深入地了解了数据库的设计和应用，对我以后的工作和学习都有很大的帮助。我会继续学习和探索数据库相关的知识，不断提高自己的能力。
